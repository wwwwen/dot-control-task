# Dot control task

This is a demo task of moving a dot with 0-100% control. Matlab and psychtoolbox are required to run the program. 

If you use this task for your own study, please cite the follow papers:

Wen, W., & Haggard, P. (2018). Control changes the way we look at the world. Journal of Cognitive Neuroscience, 30(4), 603–619. https://doi.org/10.1162/jocn_a_01226

Wen, W., Shibata, H., Ohata, R., Yamashita, A., Asama, H., & Imamizu, H. (2020). The active sensing of control. iScience, 23(5), 101112. https://doi.org/10.1016/j.isci.2020.101112

