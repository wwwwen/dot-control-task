%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%one dot control judgment task
%each trial lasts for 2s after 
%the onset of the first mouse movement
%conditions: control level 0%-100% on 10% step
%repeats: 10 times
%total number of trial: 110
%practice: 0%, 60%, 100% control * 2 repeats
%last updated on 7/Feb/2020 by Wen
%
%note: set "flg.dummyInput" to 1 if you use the mouse.
%      the joystick part is for a customized joystick
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Joystick or Mouse
flg.dummyInput = 1; % 0:joystick, 1:mouse
%start input
SubName = input(['Input the name of the participant:'], 's');
if isempty(SubName)
    return;
end

sessions = [0 1]; %0 = practice 1 = actual session

% set parameters
home_path = pwd;
bgColor =  [0 0 0]; %background colour, black
stimuliColor =[255 255 255]; %dot colour, white
textColor = [125 125 125];
stimuliSize = 40; %dot size
fontsize = 20; %font size
displayFreq = 60; %refresh rate of the monitor
trialDuration = 2; %trial duraction (sec)
actualControl = [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1]; %control levels
repeatTimes = 10; %repeats 
practiceControl = [0 0.6 1]; %control levels for practice
practiceRepeatTimes = 2; %repeats for practice
  
%define a trial
trial = struct('actualControl', 0, 'response', -1);

%title line of the result file
recordTitleStr = 'participant,trialNo.,actualControl,response';

%read other's motion
load('otherMotionParticipant.mat');
load('randomMotionIDRange.mat');
maxMotionNumber = length(otherMotionParticipant);
edgeArea = 0.2; % search for a motion to bring the dot back the screen if the dot enters the edge zone
edgeSearchInterval = 120; 


%keyboard
% Enable unified mode of KbName, so KbName accepts identical key names on
% all operating systems:
KbName('UnifyKeyNames');
DisableKeysForKbCheck([240, 243, 242, 244]);  
kbd.esc = KbName('ESCAPE');
kbd.space = KbName('SPACE');
kbd.yes = KbName('q');
kbd.no = KbName('w');

try   
    % open a new window
    AssertOpenGL;
    % removes the blue screen flash and minimize extraneous warnings.
    Screen('Preference', 'SkipSyncTests', 1);
    Screen('Preference', 'VisualDebugLevel', 3);
    Screen('Preference', 'SuppressAllWarnings', 1);
    
    screenNumber = max(Screen('Screens'));
    [win, rect] = Screen('OpenWindow', screenNumber, bgColor);
    [ centerPos(1) centerPos(2)] = RectCenter(rect);
    maxAllowedMotionX = centerPos(1) * 0.9; %to prevent from a sudden jumping of the mouse
    maxAllowedMotionY = centerPos(2) * 0.9; 
    scr.rect    = rect;
    scr.width   = rect(3) - rect(1);
    scr.height  = rect(4) - rect(2);
    
    % initialize joystick
    if flg.dummyInput == 0
        tmpJoyCaps = getJoyCaps(0);   %0:JoystickID_1, 1:JoystickID_2
        joy.scaleX = scr.width / (tmpJoyCaps.wXmax - tmpJoyCaps.wXmin);
        joy.scaleY = scr.height / (tmpJoyCaps.wYmax - tmpJoyCaps.wYmin);
        joy.offsetX = scr.width/2;
        joy.offsetY = scr.height/2;
    end
    Screen('TextFont',win, 'Arial');
    Screen('TextSize',win, fontsize);
    %DrawFormattedText(win, 'Press space to start the first practice session', 'center', 'center', stimuliColor);
    pracStr = 'Press space to start';
    DrawTx(win, rect(3)/2, rect(4)/2, pracStr, stimuliColor);

    Screen('Flip', win);
    flag = true;
    while flag
        [ keyIsDown, timeSecs, keyCode ] = KbCheck;
        if keyIsDown
            if keyCode(kbd.space)
                flag = false;
            end
            %KbReleaseWait;
        end
    end
    Screen('Flip', win);        
    
    
    
    % duration of each frame
    ifi = Screen('GetFlipInterval', win);
    if abs(ifi*displayFreq - 1) > 1
        disp(strcat('Please check the parameter of displayFreq(current =', num2str(displayFreq), '), ifi of the monitor is ', num2str(ifi)));
        return;
    end
    HideCursor;
    
    
    tmpdatestr = datestr(now);
    tmpdatestr = strrep(tmpdatestr,':','-');
    tmpdatestr = strrep(tmpdatestr,' ','_');
    
    escapeFlag = false;
    for sessionInd = 1:length(sessions)
        if escapeFlag             
            %save(fullfile(home_path,'output',mouseFileName), 'mouseMovement');%, 'visualIntervalList');
            %fclose(fid);
            break;
        end
        
        if sessions(sessionInd) == 0 %practice
            SaveFileName = strcat(SubName, '_', tmpdatestr, '_onedot_results_practice.csv');
            mouseFileName = strcat(SubName, '_', tmpdatestr, '_onedot_mouse_practice.mat');
            
            trialSum = length(practiceControl) * practiceRepeatTimes;
            trialList = repmat(trial, 1, trialSum);
            for trialInd = 1:trialSum
                tmpInd = mod(trialInd, length(practiceControl));
                if tmpInd == 0
                    tmpInd = length(practiceControl);
                end
                trialList(trialInd).actualControl = practiceControl(tmpInd);
            end
        else %actual session
            SaveFileName = strcat(SubName, '_', tmpdatestr, '_onedot_results_', num2str(sessionInd-1), '.csv');
            mouseFileName = strcat(SubName, '_', tmpdatestr, '_onedot_mouse_', num2str(sessionInd-1), '.mat');
            trialSum = length(actualControl) * repeatTimes;
            trialList = repmat(trial, 1, trialSum);
            for trialInd = 1:trialSum
                tmpInd = mod(trialInd, length(actualControl));
                if tmpInd == 0
                    tmpInd = length(actualControl);
                end
                trialList(trialInd).actualControl = actualControl(tmpInd);
            end
        end
        %randomize
        randomInd = randperm(trialSum);
        trialList = trialList(randomInd);
        %recording file
        fid = fopen(SaveFileName, 'wt');
        fprintf(fid, '%s\n', recordTitleStr);            
        
	%record mouse movement
	%only record for the actual session
	clear mouseMovement;
        mouseMovement = zeros(displayFreq * trialDuration*trialSum, 9);  %session, trialno, frameCounter, time code, x-shift, y-shift, dot-x, dot-y, control level
        changeRecordVal = 0;
        mouseInd = 1;  
        
        if sessions(sessionInd) == 0 %practice
            %show a msg of "practice" for 2s
            pracDisp = 'Practice';
            DrawTx(win, rect(3)/2, rect(4)/2, pracDisp, stimuliColor);
            Screen('Flip', win);
            WaitSecs(2);
            sessionStartTime = datevec(now);
        else
            mriDisp = 'Ready to start (press space)';
            DrawTx(win, rect(3)/2, rect(4)/2, mriDisp, stimuliColor);
            Screen('Flip', win);
            flag = true;
            while flag
                [ keyIsDown, timeSecs, keyCode ] = KbCheck;
                if keyIsDown
                    if keyCode(kbd.space)
                        flag = false;
                    elseif keyCode(kbd.esc)
                        disp('------program terminated');
                        flag = false;
                        escapeFlag = true;
                        return;                        
                    end
                    %KbReleaseWait;
                end
            end
            Screen('Flip', win);
            sessionStartTime = datevec(now);
        end

        %%%%%%%%%%%%%%%%%%%%trial start%%%%%%%%%%%%%%%%%%%%%%%%
        for trialInd = 1:trialSum  %repeat trials
            if escapeFlag 
                break;
            end
            trialStart = datevec(now);
            
            %initial parameters for a new trial
            %%% get joystick input
            if flg.dummyInput == 0
                %%% how to check:  type "test=getJoyVal(0)" matlab prompt
                tmpJoy = getJoyVal(0);  %0:joy1, 1:joy2
                joy.mX = tmpJoy.wXpos; joy.mY = tmpJoy.wYpos; buttons = tmpJoy.wButtons;
                % save position
                x = joy.scaleX * (joy.mX-joy.offsetX) + joy.offsetX; % modify gain
                y = joy.scaleY * (joy.mY-joy.offsetY) + joy.offsetY; % modify gain
                while any(buttons)
                    tmpJoy = getJoyVal(0);  %0:joy1, 1:joy2
                    joy.mX = tmpJoy.wXpos; joy.mY = tmpJoy.wYpos; buttons = tmpJoy.wButtons;
                    % save position
                    x = joy.scaleX * joy.mX; % modify gain
                    y = joy.scaleY * joy.mY; % modify gain
                end
            else
                [x1, y1, buttons] = GetMouse;
                while any(buttons)
                    [x, y, buttons] = GetMouse;
                end
            end %if

            SetMouse(centerPos(1), centerPos(2));
            x = centerPos(1);
            y = centerPos(2);
            frameCounter = 0;
            lastX = centerPos(1);
            lastY = centerPos(2);
            dotX = centerPos(1);
            dotY = centerPos(2);
            tmpR = mod(round(rand(1)*length(randomMotionIDRange)), length(randomMotionIDRange)) + 1;   
            randomMotionInd =  randomMotionIDRange(tmpR);  
            edgeSearchCounter = 0;
            isEdgeSearchedFlag = false;
            controlRatio = trialList(trialInd).actualControl;

           
            edgeSearchCounter = 0;
            isEdgeSearchedFlag = false;
            startTime = datevec(now);
            currentTime = startTime;
            startMovingFlag = false;    
            while ~startMovingFlag || (startMovingFlag && etime(currentTime, startTime) <= trialDuration)
                frameCounter = frameCounter + 1;
                currentTime = datevec(now);
                x1 = x;
                y1 = y;
                if flg.dummyInput == 0
                    tmpJoy = getJoyVal(0);  %0:joy1, 1:joy2
                    joy.mX = tmpJoy.wXpos; joy.mY = tmpJoy.wYpos; buttons = tmpJoy.wButtons;
                    % save position
                    x = joy.scaleX * joy.mX; % modify gain
                    y = joy.scaleY * joy.mY; % modify gain
                else
                    [x, y, buttons] = GetMouse;
                end
                x_shift = x-x1; %check mouse movement at each frame
                y_shift = y-y1;

                if ~startMovingFlag && ((x_shift ~= 0) || (y_shift ~= 0))
                    startMovingFlag = true;
                    startTime = datevec(now);
                end

                %update the position of the stimulus
                if (y_shift ~= 0 || x_shift ~= 0) && (abs(x_shift) < maxAllowedMotionX  && abs(y_shift) < maxAllowedMotionY )
                    controlRatio = trialList(trialInd).actualControl;
                    edgeSearchCounter = edgeSearchCounter + 1;
                    x_move = 0;
                    y_move = 0;

                    %read pre-record motion
                    tmpTShiftX = otherMotionParticipant(randomMotionInd, 1);
                    tmpTShiftY = otherMotionParticipant(randomMotionInd, 2);
                    
                    
%%%%%%%%%%%%%%%algorithm of combing motion%%%%%%%%%

                   tmpAngle = atan2(tmpTShiftY, tmpTShiftX); %direction of pre-recorded motion
                   selfAngle = atan2(y_shift, x_shift); %direction of self motion
                   %mix the moving direction based on the level of control
                   angleDifference = abs(selfAngle - tmpAngle);
                   if angleDifference > pi
                       angleDifference = 2*pi - angleDifference;
                   end
                   angularBias = angleDifference * (1-controlRatio);
                   if (abs(selfAngle - tmpAngle) <= pi && tmpAngle < selfAngle ) || (abs(selfAngle - tmpAngle) > pi && selfAngle < tmpAngle)  %clockwise
                        cAngle = selfAngle - angularBias;
                   else %counterclockwise
                       cAngle = selfAngle + angularBias;
                   end
                   if cAngle < -pi
                       cAngle = cAngle + 2*pi;
                   elseif cAngle > pi
                       cAngle = cAngle - 2*pi;
                   end
                     
                    x_move = hypot(x_shift, y_shift) * cos(cAngle);
                    y_move = hypot(x_shift, y_shift) * sin(cAngle);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




                    %update dot motion                  
                    dotX = dotX + x_move;
                    dotY = dotY + y_move;

                   %determine the next pre-recorded motion based on the dot's current posittion
		   %if the dot is in the edge zone, search for a new section that can potentially bring it towards the centre of the screen
                        if controlRatio < 1
                           if dotX/centerPos(1)/2 < edgeArea
                               horizontalEdge = -1; %close to left border
                           elseif dotX/centerPos(1)/2 > (1-edgeArea)
                               horizontalEdge = 1; %close to right border
                           else
                               horizontalEdge = 0;
                           end
                           if dotY/centerPos(2)/2 < edgeArea
                               verticalEdge = -1; %close to left border
                           elseif dotY/centerPos(2)/2 > (1-edgeArea)
                               verticalEdge = 1; %close to right border
                           else
                               verticalEdge = 0;
                           end


                           if (horizontalEdge == 0) && (verticalEdge == 0)
                               randomMotionInd = randomMotionInd + 1;
                           end

                           if ~ ((horizontalEdge == 0) && (verticalEdge == 0))
                               clear row;
                               if ~isEdgeSearchedFlag || (edgeSearchCounter >= edgeSearchInterval)
                                   if (horizontalEdge == -1) && (verticalEdge == -1) %left upper corner
                                       [row,col,v] = find((otherMotionParticipant(:,3) < edgeArea) & (otherMotionParticipant(:,4) < edgeArea));
                                   elseif (horizontalEdge == -1) && (verticalEdge == 1) %left lower corner
                                       [row,col,v] = find((otherMotionParticipant(:,3) < edgeArea) & (otherMotionParticipant(:,4) > (1-edgeArea)));          
                                   elseif (horizontalEdge == 1) && (verticalEdge == -1) %right upper corner
                                       [row,col,v] = find((otherMotionParticipant(:,3) > (1-edgeArea)) & (otherMotionParticipant(:,4) < edgeArea));    
                                   elseif (horizontalEdge == 1) && (verticalEdge == 1) %right lower corner
                                       [row,col,v] = find((otherMotionParticipant(:,3) > (1-edgeArea)) & (otherMotionParticipant(:,4) > (1-edgeArea)));   
                                   elseif (horizontalEdge == -1) && (verticalEdge == 0) %left border
                                       [row,col,v] = find((otherMotionParticipant(:,3) < edgeArea) & (otherMotionParticipant(:,4) >= edgeArea) & (otherMotionParticipant(:,4) <= (1-edgeArea)));   
                                   elseif (horizontalEdge == 1) && (verticalEdge == 0) %right border
                                       [row,col,v] = find((otherMotionParticipant(:,3) > (1-edgeArea)) & (otherMotionParticipant(:,4) >= edgeArea) & (otherMotionParticipant(:,4) <= (1-edgeArea)));  
                                   elseif (horizontalEdge == 0) && (verticalEdge == -1) %upper border
                                       [row,col,v] = find((otherMotionParticipant(:,3) >= edgeArea) & (otherMotionParticipant(:,3) <= (1-edgeArea))& (otherMotionParticipant(:,4) < edgeArea));   
                                   elseif (horizontalEdge == 0) && (verticalEdge == 1) %lower border
                                       [row,col,v] = find((otherMotionParticipant(:,3) >= edgeArea) & (otherMotionParticipant(:,3) <= (1-edgeArea))& (otherMotionParticipant(:,4) > (1-edgeArea))); 
                                   end

                                   if ~ isempty(row)
                                        tmpRandomInd = randperm(length(row));
                                        randomMotionInd = row(tmpRandomInd(1));
                                   else
                                       randomMotionInd = randomMotionInd + 1;
                                   end
                                   edgeSearchCounter = 0;
                                   isEdgeSearchedFlag = true;
                               else
                                   randomMotionInd = randomMotionInd + 1;                               
                               end
                           end
                        end
                   
                        if randomMotionInd > maxMotionNumber
                            tmpR = mod(round(rand(1)*length(randomMotionIDRange)), length(randomMotionIDRange)) + 1;   
                            randomMotionInd =  randomMotionIDRange(tmpR);        
                        end


                  %stop the dot at the border of the screen
                  if dotX < stimuliSize/2
                      dotX = stimuliSize/2;
                  end
                  if dotX > 2*centerPos(1)-stimuliSize/2
                      dotX = 2 * centerPos(1) -stimuliSize/2;
                  end
                  if dotY < stimuliSize/2
                      dotY = stimuliSize/2;
                  end
                  if dotY > 2*centerPos(2)-stimuliSize/2
                      dotY = 2 * centerPos(2)-stimuliSize/2;
                  end     
                     %record mouse movement and amplitude
                    %%session, trialno, frameCounter, time code, x-shift, y-shift, dot-x, dot-y, control level
                    mouseMovement(mouseInd, 1) = sessionInd;
                    mouseMovement(mouseInd, 2) = trialInd;
                    mouseMovement(mouseInd, 3) = frameCounter;
                    mouseMovement(mouseInd, 4) = etime(currentTime, startTime);
                    mouseMovement(mouseInd, 5) = x_shift;
                    mouseMovement(mouseInd, 6) = y_shift;
                    mouseMovement(mouseInd, 7) = dotX;
                    mouseMovement(mouseInd, 8) = dotY;
                    mouseMovement(mouseInd, 9) = controlRatio;
                    mouseInd = mouseInd + 1;   
                elseif ~(abs(x_shift) < maxAllowedMotionX  && abs(y_shift) < maxAllowedMotionY )
                        %for debug
                        disp(strcat('maxAllowedMotionX = ', num2str(maxAllowedMotionX), '; x_shift=', num2str(x_shift), ': maxAllowedMotionY = ', num2str(maxAllowedMotionY),'; y_shift=', num2str(y_shift)));
                end

                %after all the calculation, draw the dot
                Screen('FillOval', win, stimuliColor, [dotX-stimuliSize/2, dotY-stimuliSize/2, dotX+stimuliSize/2, dotY+stimuliSize/2]);     
                Screen('Flip', win);
                
                 if flg.dummyInput== 1
                    if (x <= 100) || (x >= centerPos(1)*2-100) || (y <= 100) || (y >= centerPos(2)*2-100)
                        SetMouse(centerPos(1), centerPos(2));
                        x = centerPos(1);
                        y = centerPos(2);
                    end
                end
            end

            %show question
            Screen('Flip', win);
            msg1 = 'Did you feel that you controlled the dot moving direction?(q=yes, w=no)';
            DrawTx(win, rect(3)/2, rect(4)/2, msg1, stimuliColor)
            Screen('Flip', win);  
            responseFlag = false;
            while ~responseFlag
                [ keyIsDown, timeSecs, keyCode ] = KbCheck;
                if keyIsDown
                    if keyCode(kbd.yes) 
                        responseFlag = true;
                        trialList(trialInd).response = 1;
                    elseif keyCode(kbd.no) 
                        responseFlag = true;
                        trialList(trialInd).response = 0;
                    elseif keyCode(kbd.esc)
                        escapeFlag = true;
                        break;
                    end
                    KbReleaseWait;
                end
            end
            Screen('Flip', win);             

            resultsStr = strcat(SubName, ',', ...      
                num2str(trialInd), ',',...
                num2str(trialList(trialInd).actualControl), ',',...
                num2str(trialList(trialInd).response));
            fprintf(fid, '%s\n', resultsStr); 
        end
        %%%%%%%%%%%%%%%%%%%%end of each trial%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

       % save(fullfile(home_path,'output',mouseFileName), 'mouseMovement');%, 'visualIntervalList');
        save(mouseFileName, 'mouseMovement');
        fclose(fid);
        
    end

    %%%%%%%%%%%%%%%%end of each session%%%%%%%%%%%%%%%%%%%
    if sessionInd < length(sessions)
        msg = 'This session has finished.';
    else
        msg = 'All sessions finished. ';
    end
    DrawTx(win, rect(3)/2, rect(4)/2, msg, stimuliColor);
    Screen('Flip', win);
    WaitSecs(2);
    Screen('CloseAll');
    
catch
   Screen('CloseAll');
   sca;
   psychrethrow(psychlasterror);
end
